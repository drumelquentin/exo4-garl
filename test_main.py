import unittest
from main import calculate_area_rectangle

class TestAreaCalculation(unittest.TestCase):

    def test_positive_values(self):
        self.assertEqual(calculate_area_rectangle(5, 10), 50, "Should correctly calculate the rectangle area")

    def test_zero_value(self):
        self.assertEqual(calculate_area_rectangle(0, 10), 0, "Area should be zero if one side is zero")
        self.assertEqual(calculate_area_rectangle(10, 0), 0, "Area should be zero if one side is zero")

    def test_negative_values(self):
        with self.assertRaises(ValueError):
            calculate_area_rectangle(-5, 10)
        with self.assertRaises(ValueError):
            calculate_area_rectangle(5, -10)

if __name__ == '__main__':
    unittest.main()
